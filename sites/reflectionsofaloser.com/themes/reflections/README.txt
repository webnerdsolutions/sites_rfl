
Introduction to HFCC CIS

The cisdrupal theme was adapted directly from the Basic theme developed by
Raincity Studios. See http://drupal.org/project/basic for more information.

__________________________________________________________________________________________

Installation

- Unpack the downloaded file and place the cisdrupal folder in your Drupal installation under one of the following locations:

    * sites/all/themes
    * sites/default/themes
    * sites/example.com/themes 

- Log in as an administrator on your Drupal site and go to Administer > Site building > Themes (admin/build/themes) and
  enable the cisdrupal theme.

__________________________________________________________________________________________


Holy Grail Layout

The layout used in Basic, like in Zen, is the Holy Grail method. You can
have a detailed information about this method here :
http://www.alistapart.com/articles/holygrail

The purpose of this method is to have a minimal markup for an ideal display.
For accessibility and search engine optimization, the best order to display
a page is the following :

	1. header
	2. content
	3. sidebars
	4. footer

This is how the page template is buit in basic, and it works in fluid and fixed layout. 

__________________________________________________________________________________________